package com.app.foodcalorieschecker;

import android.os.AsyncTask;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class HttpHandler extends AsyncTask<File, Void, Map> {
    private static final String BASE_URL = "https://api.logmeal.es/v2";
    private static final String RECOGNITION_API_END_POINT = BASE_URL + "/image/recognition/complete/v1.0?skip_types=[1,3]&language=eng";
    private static final String NUTRITIONAL_INFO_API_END_POINT = BASE_URL + "/nutrition/recipe/nutritionalInfo/v1.0?language=eng";
    private static final String API_USER_TOKEN = "a4e8e47a79a1caafc734b50c168271b9ba191506";

    private static final MediaType MEDIA_TYPE_FORMDATA = MediaType.parse("multipart/form-data");
    private static final MediaType MEDIA_TYPE_JSON = MediaType.parse("application/json");
    OkHttpClient client = new ApiClient().getClientInstance();

    @Override
    protected Map<String, String> doInBackground(File... params) {
        Map<String, String> foodData = new HashMap<>();
        Map<String, String> nutritionData = new HashMap<>();
        try {
            Request recognitionRequest = new ApiClient().getCustomPostRequest(RECOGNITION_API_END_POINT, API_USER_TOKEN, MEDIA_TYPE_FORMDATA, MainActivity.selectedFile);
            Response recognitionResponse = client.newCall(recognitionRequest).execute();
            String recognitionResponseBody = recognitionResponse.body().string();
            foodData = JSONParser.parseFoodInfo(recognitionResponseBody);
            //foodData = JSONParser.parseFoodInfo("{\"foodFamily\":[{\"id\":10,\"name\":\"bread\",\"prob\":0.80078125}],\"foodType\":[{\"id\":1,\"name\":\"food\"},{\"id\":1,\"name\":\"food\"},{\"id\":1,\"name\":\"food\"},{\"id\":1,\"name\":\"food\"},{\"id\":1,\"name\":\"food\"},{\"id\":1,\"name\":\"food\"}],\"imageId\":1364885,\"model_versions\":{\"drinks\":\"v1.0\",\"foodType\":\"v1.0\",\"foodgroups\":\"v1.0\",\"foodrec\":\"v1.0\",\"ingredients\":\"v1.0\"},\"recognition_results\":[{\"id\":168,\"name\":\"pizza\",\"prob\":0.5171287059783936,\"subclasses\":[{\"id\":1530,\"name\":\"focaccia ai pomodori\",\"prob\":0.228515625}]},{\"id\":1530,\"name\":\"focaccia ai pomodori\",\"prob\":0.20809650421142578,\"subclasses\":[]},{\"id\":600,\"name\":\"bread with tomato and ham\",\"prob\":0.016424283385276794,\"subclasses\":[]},{\"id\":1005,\"name\":\"bread with sobrassada\",\"prob\":0.006506489589810371,\"subclasses\":[]},{\"id\":533,\"name\":\"vegetable lasagna\",\"prob\":0.006016679108142853,\"subclasses\":[]},{\"id\":396,\"name\":\"sole with onion\",\"prob\":0.005436548963189125,\"subclasses\":[]}]}");

            String nutritionalRequestParameters = String.format("{\n  \"imageId\": %s,\n  \"class_id\": %s\n}", foodData.get("foodImageId"), foodData.get("foodClassId"));

            Request nutritionRequest = new ApiClient().getCustomPostRequest(NUTRITIONAL_INFO_API_END_POINT, API_USER_TOKEN, MEDIA_TYPE_JSON, nutritionalRequestParameters);
            Response nutritionResponse = client.newCall(nutritionRequest).execute();
            String nutritionResponseBody = nutritionResponse.body().string();
            nutritionData = JSONParser.parseNutritionInfo(nutritionResponseBody);
            //nutritionData = JSONParser.parseNutritionInfo("{\"foodName\":\"pizza\",\"hasNutritionalInfo\":true,\"ids\":168,\"imageId\":1364885,\"nutritional_info\":{\"calories\":701.9,\"dailyIntakeReference\":{\"CHOCDF\":{\"label\":\"Carbs\",\"level\":\"HIGH\",\"percent\":44.990981165671165},\"ENERC_KCAL\":{\"label\":\"Energy\",\"level\":\"NONE\",\"percent\":34.10113830889581},\"FASAT\":{\"label\":\"Saturated\",\"level\":\"HIGH\",\"percent\":31.164453872938235},\"FAT\":{\"label\":\"Fat\",\"level\":\"HIGH\",\"percent\":38.023813771298215},\"NA\":{\"label\":\"Sodium\",\"level\":\"HIGH\",\"percent\":89.64},\"PROCNT\":{\"label\":\"Protein\",\"level\":\"NONE\",\"percent\":14.445654828102326},\"SUGAR\":{\"label\":\"Sugars\",\"level\":\"MEDIUM\",\"percent\":15.968000000000002}},\"totalNutrients\":{\"CA\":{\"label\":\"Calcium\",\"quantity\":181.65,\"unit\":\"mg\"},\"CHOCDF\":{\"label\":\"Carbs\",\"quantity\":104.18,\"unit\":\"g\"},\"CHOLE\":{\"label\":\"Cholesterol\",\"quantity\":22.4,\"unit\":\"mg\"},\"ENERC_KCAL\":{\"label\":\"Energy\",\"quantity\":701.9,\"unit\":\"kcal\"},\"FAMS\":{\"label\":\"Monounsaturated fats\",\"quantity\":12.05,\"unit\":\"g\"},\"FAPU\":{\"label\":\"Polyunsaturated\",\"quantity\":2.3,\"unit\":\"g\"},\"FASAT\":{\"label\":\"Saturated\",\"quantity\":5.88,\"unit\":\"g\"},\"FAT\":{\"label\":\"Fat\",\"quantity\":21.74,\"unit\":\"g\"},\"FATRN\":{\"label\":\"Trans fat\",\"quantity\":0.0,\"unit\":\"g\"},\"FE\":{\"label\":\"Iron\",\"quantity\":7.28,\"unit\":\"mg\"},\"FIBTG\":{\"label\":\"Fiber\",\"quantity\":6.3,\"unit\":\"g\"},\"FOLAC\":{\"label\":\"Folic acid\",\"quantity\":192.5,\"unit\":\"\\u00b5g\"},\"FOLDFE\":{\"label\":\"Folate equivalent (total)\",\"quantity\":470.7,\"unit\":\"\\u00b5g\"},\"FOLFD\":{\"label\":\"Folate (food)\",\"quantity\":143.2,\"unit\":\"\\u00b5g\"},\"K\":{\"label\":\"Potassium\",\"quantity\":559.05,\"unit\":\"mg\"},\"MG\":{\"label\":\"Magnesium\",\"quantity\":54.04,\"unit\":\"mg\"},\"NA\":{\"label\":\"Sodium\",\"quantity\":1344.6,\"unit\":\"mg\"},\"NIA\":{\"label\":\"Niacin (B3)\",\"quantity\":10.24,\"unit\":\"mg\"},\"P\":{\"label\":\"Phosphorus\",\"quantity\":294.19,\"unit\":\"mg\"},\"PROCNT\":{\"label\":\"Protein\",\"quantity\":22.3,\"unit\":\"g\"},\"RIBF\":{\"label\":\"Riboflavin (B2)\",\"quantity\":0.94,\"unit\":\"mg\"},\"SUGAR\":{\"label\":\"Sugars\",\"quantity\":4.99,\"unit\":\"g\"},\"SUGAR.added\":{\"label\":\"Sugars, added\",\"quantity\":0.0,\"unit\":\"g\"},\"THIA\":{\"label\":\"Thiamin (B1)\",\"quantity\":1.46,\"unit\":\"mg\"},\"TOCPHA\":{\"label\":\"Vitamin E\",\"quantity\":3.83,\"unit\":\"mg\"},\"VITA_RAE\":{\"label\":\"Vitamin A\",\"quantity\":79.02,\"unit\":\"\\u00b5g\"},\"VITB12\":{\"label\":\"Vitamin B12\",\"quantity\":0.65,\"unit\":\"\\u00b5g\"},\"VITB6A\":{\"label\":\"Vitamin B6\",\"quantity\":0.25,\"unit\":\"mg\"},\"VITC\":{\"label\":\"Vitamin C\",\"quantity\":8.68,\"unit\":\"mg\"},\"VITD\":{\"label\":\"Vitamin D\",\"quantity\":4.65,\"unit\":\"\\u00b5g\"},\"VITK1\":{\"label\":\"Vitamin K\",\"quantity\":14.67,\"unit\":\"\\u00b5g\"},\"ZN\":{\"label\":\"Zinc\",\"quantity\":2.3,\"unit\":\"mg\"}}},\"serving_size\":295.35}");
        } catch (Exception e) {
            e.printStackTrace();
        }
        foodData.putAll(nutritionData);
        MainActivity.foodNutrition = foodData;
        System.out.println(foodData);
        return foodData;
    }

    @Override
    protected void onPostExecute(Map foodNutrition) {
        super.onPostExecute(foodNutrition);
        MainActivity.foodNutrition = foodNutrition;
    }
}