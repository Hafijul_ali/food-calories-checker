package com.app.foodcalorieschecker;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;


public class ApiClient {
    private OkHttpClient client = null;

    public OkHttpClient getClientInstance() {
        if (client == null)
            client = new OkHttpClient();
        return client;
    }

    public Request getCustomPostRequest(String API_END_POINT, String API_USER_TOKEN, MediaType MEDIA_TYPE, File file) {
        Request request = null;

        try {
            RequestBody requestBody = new MultipartBody.Builder().setType(MultipartBody.FORM)
                    .addFormDataPart("image", file.getName(), RequestBody.create(MEDIA_TYPE, file))
                    .build();

            request = new Request.Builder()
                    .url(API_END_POINT)
                    .method("POST", requestBody)
                    .addHeader("Content-Type", "multipart/form-data")
                    .addHeader("Accept", "application/json")
                    .header("Authorization", "Bearer " + API_USER_TOKEN)
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return request;
    }

    public Request getCustomPostRequest(String API_END_POINT, String API_USER_TOKEN, MediaType MEDIA_TYPE, String data) {
        Request request = null;

        try {
            RequestBody requestBody = RequestBody.create(MEDIA_TYPE, data);

            request = new Request.Builder()
                    .url(API_END_POINT)
                    .method("POST", requestBody)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Accept", "application/json")
                    .header("Authorization", "Bearer " + API_USER_TOKEN)
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return request;
    }

}
