package com.app.foodcalorieschecker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class JSONParser {

    public static Map<String, String> parseFoodInfo(String recognitionResponse) throws JSONException {
        Map<String, String> foodData = new HashMap<>();
        JSONObject recognitionResponseJson = new JSONObject(recognitionResponse);

        String imageId = String.valueOf(recognitionResponseJson.get("imageId"));
        JSONArray recognitionResult = recognitionResponseJson.getJSONArray("recognition_results");
        JSONObject foodClass = (JSONObject) recognitionResult.get(0);
        String classId = String.valueOf(foodClass.get("id"));
        String className = String.valueOf(foodClass.get("name"));
        String recognitionProbability = String.valueOf(foodClass.get("prob"));

        foodData.put("foodImageId", imageId);
        foodData.put("foodClassId", classId);
        foodData.put("foodClassName", className);
        foodData.put("recognitionProbability", recognitionProbability);

        return foodData;
    }

    public static Map<String, String> parseNutritionInfo(String nutritionResponse) throws JSONException {
        Map<String, String> nutritionData = new HashMap<>();
        JSONObject nutritionResponseJson = new JSONObject(nutritionResponse);

        boolean hasNutritionalInfo = nutritionResponseJson.getString("hasNutritionalInfo").equals("true");
        String serving_size = nutritionResponseJson.getString("serving_size");

        nutritionData.put("hasNutritionalInfo", String.valueOf(hasNutritionalInfo));
        nutritionData.put("servingSize", serving_size);
        if (hasNutritionalInfo) {
            JSONObject nutritionalInfo = nutritionResponseJson.getJSONObject("nutritional_info");
            JSONObject totalNutrients = nutritionalInfo.getJSONObject("totalNutrients");
            String calories = nutritionalInfo.getString("calories");

            JSONObject energy = totalNutrients.getJSONObject("ENERC_KCAL");
            JSONObject protein = totalNutrients.getJSONObject("PROCNT");
            JSONObject fat = totalNutrients.getJSONObject("FAT");
            JSONObject fiber = totalNutrients.getJSONObject("FIBTG");
            JSONObject carbohydrates = totalNutrients.getJSONObject("CHOCDF");

            nutritionData.put("calories", calories);
            nutritionData.put("energy", nutrientToString(energy));
            nutritionData.put("protein", nutrientToString(protein));
            nutritionData.put("fat", nutrientToString(fat));
            nutritionData.put("fiber", nutrientToString(fiber));
            nutritionData.put("carbohydrates", nutrientToString(carbohydrates));
        }

        return nutritionData;
    }

    private static String nutrientToString(JSONObject nutrient) throws JSONException {
        return String.format("%s %s %s", nutrient.getString("label"), nutrient.getString("quantity"), nutrient.getString("unit"));
    }

}